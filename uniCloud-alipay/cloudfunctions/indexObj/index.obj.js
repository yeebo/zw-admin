const db = uniCloud.database()
const dbjql = uniCloud.databaseForJQL()
module.exports = {
	_before: function () { // 通用预处理器

	},
	async getCategory(){
		let res =await db.collection("zw-categories").get()
		return res
	},
	getListData(){
		let res1 =dbjql.collection("zw-categories").getTemp()
		let res2 =dbjql.collection("zw-categories-class").getTemp()
		let res = dbjql.collection(res1,res2).get()
		return res
	}
	
	
}
